---
title: VA Tenant's Rights
subtitle: Information for VA Tenants
comments: True
---

#On This Page


# VA Law 

The Virginia Residential Landlord and Tenant Act (“VRLTA”), Sections 55.1-1200 through 55.1-1262 of the Code of Virginia, provides the rights and responsibilities of landlords and tenants in Virginia.

Generally speaking, the VRLTA, applies to all residential tenancies (leased residential premises) unless the landlord is eligible to opt out, and states this in a written lease. The VRLTA also applies to stays in hotels, motels, or boarding houses if the tenant has been renting for more than 90 days or has a written lease for more than 90 days. Even if your rental is not covered by the VRLTA, there may be other state laws that apply to your situation. If you do not know which law applies, [you should seek advice from an attorney.](page/attorney)

# Your Rights as a Tenant

## Under Virginia Law, tenants have certain rights when they move in, while they are renting, and before they can be evicted.

### Moving In

   [LEASE AGREEMENTS](#lease-agreements)

   [SECURITY DEPOSITS](#security-deposits)

### Currently Renting 

   [REPAIRS AND MAINTENANCE](#repairs-and-maintenance)

   [RENT PAYMENT](#rent-payment)

   [BREACH OF LEASE](#breach-of-lease)

### Eviction or Moving Out 
 
   [ENDING YOUR LEASE](#ending-your-lease)

   [NOTICE OF EVICTION](#notice-of-eviction)

--- 

# Moving In

---
## LEASE AGREEMENTS

Most landlords will have you sign a lease before you move in. A lease is a contract stating what the landlord will do and what you as the renter will have to do. As the law will generally make you follow all the terms of the lease, make sure you clearly understand what you have agreed to do. Pay careful attention to the following items: 
 
   How much the rent will be per mont? 
   
   How much the rent will be per month.				
   How much the security deposit will be, if there is one.				
   What day the rent is due and when it is considered late.				
   How much is the late fee, if you are late with the payment. 
   How long the lease runs; month to month, six months, a year.
   -  How many days advance notice you have to give if you wish to move.
   -  Whether the electric, heat, water and sewer are included in the rent.
   -  Whether a refrigerator, stove, air conditioner, or other appliances are provided by the landlord.
   -  What you must do to get repairs made.
   - Any specific rules or other charges.

Although most landlords have a lease, there is no requirement that there be anything in writing. If you simply pay rent once a month, then it is called a month to month tenancy and starts again each month. Either you or the landlord can end the tenancy by giving written notice at least 30 days before the next rent payment is due. And, as each month is a new tenancy, the landlord must give the same 30 day notice if he or she wants to raise the rent or make other changes. 

## SECURITY DEPOSITS

Most landlords will make you pay a security deposit before you move in. Under the VRLTA the deposit cannot be more than two months rent. The deposit is held by the landlord until you move out to cover the cost of any damages you may make to the apartment while you live there or any outstanding rent or other charges that you owe. If you leave owing no money and the premises are clean and in generally the same condition as when you first moved in, this deposit will be returned to you. However, if there are damages or money owed, the landlord will keep the Security Deposit. 			

If you are covered by the VRLTA, the landlord may be able to withhold a reasonable portion of the security deposit to cover any unpaid water, sewer, or other utilities that were your responsibility to pay directly to the utility company under the lease agreement. The landlord must first give you notice of the intent to withhold that amount. That notice may be given to you in a termination or vacating notice, or in a separate written notice at least 15 days prior to the landlord’s disposition of the security deposit. If the landlord actually pays the utility bills that were your responsibility, then the landlord must give you written confirmation of that fact within 10 days after the payment was made, along with payment to you of any balance of the security deposit owing. On the other hand, if you can provide written proof to the landlord that you actually paid the utility bills, then the landlord must properly refund the security deposit. 	

If you are covered by the VRLTA, the landlord must return the deposit or send you an itemized list of the damages or charges he or she is deducting from the deposit within 45 days of when you move out. Also under the VRLTA, if you have lived there for more than 13 months, the landlord must give you interest on the deposit as well				

If you are not covered under the VRLTA, there is no interest or specific time limit for the return of the deposit. However, if it is not returned after a reasonable amount of time, you can go to court and sue for its return. 	

### IMPORTANT TIPS:	

- Always thoroughly check the rooms, appliances, and plumbing before you move into an apartment.
 						
- As soon as you move in, make a list of all the things wrong with the apartment or house, give a copy to the landlord and keep a copy for yourself. You may also wish to take pictures of the apartment before you move in, so you have a record of the condition of the unit when you moved in. This will protect you from being charged for existing damages later.
 						
- When you are ready to move out, make an appointment with the landlord to inspect the premises together so you can agree on its condition.
 						
- If you are concerned as to return of the deposit, you may also want to take pictures when you move out so you can later prove, if need be, how you left the premises.
 						
- Always return the keys and if you expect return of the deposit, leave a forwarding address.
 						
- When you move, take everything with you in as short a period of time as possible. Property you leave can be treated as abandoned.
 						
- Finally, unless specifically agreed to by the landlord, do not use the security deposit to pay your last month’s rent as your landlord could bring an eviction action when the rent is not paid timely. 

---
# Currently Renting 
- [Repairs](#repairs-and-maintenance)
- [Rent Payment](#rent-payment)
--- 

## REPAIRS AND MAINTENANCE

1. 	Under Virginia law, unless properly agreed otherwise, all landlords must do these things: 
 			 					
    *   Follow building and housing codes affecting health and safety. 
 						
    *   Make all repairs needed to keep the place fit and habitable. 
 						
    *   Keep in good and safe working order all electrical, plumbing, sanitary, heating, ventilating, air conditioning and other facilities and appliances that the landlord supplies. 
 						
    *   Prevent or remove rodent infestations. 
 						
    *   Landlords covered by the VRLTA must also keep clean and safe any common areas used by more than one tenant. 

If something needs to be repaired that is the landlord’s responsibility, **you must notify the landlord in writing of the problem **and give him or her a reasonable time to fix it. If it is an emergency, such as lack of heat or water, your landlord should fix it immediately. This means within hours, or at most a day or two. Other repairs must be made within a reasonable time. Your letter should specify the repairs needed and a time by which to fix each problem. As you must give your landlord access to your home to make repairs, you may also want to put in the letter what times of day are best for you or how the landlord can reach you for permission to enter the premises.  

The requirement of a written notice of repairs can also be met by calling your local housing inspector and having them inspect the premises. The inspector will then send a notice to the landlord and a copy to you listing the repairs that need to be made and a time frame to make them. If the home is in too bad a state of disrepair, then it is possible that the building inspector could condemn the home. If the home is condemned then you may only have 24 hours to vacate the premises.					

If repairs aren’t made in a reasonable time, you can take your landlord to court with what is called a "rent escrow" case. At this point, it probably is best to get legal help. **To use this procedure, you pay your full rent into court within five days of the date the rent first comes due**. You fill out a "Tenant’s Assertion and Complaint" form at the General District Court for the county or city where you live. You can attach a copy of the inspection report or your repair letter to the landlord. You also can list the bad conditions on the form. To file and serve the papers will cost about $30. You may ask the clerk for "waiver of fee" if you can’t afford to pay. When you fill out the Tenant’s Assertion, you need to decide what you want the judge to do. You can ask the judge for any of these things: to order repairs completed before your rent is released to the landlord; to order repairs and return of some (or all) of the rent money to you for having to put up with the bad conditions; to order your lease ended so you can move out without paying future rent.			

After filing the Tenant’s Assertion, the court sets a hearing date and has the landlord served with a summons to appear in court. You can also ask the clerk to subpoena the building inspector if there was one, and any other witnesses who have agreed to help you. Subpoenas cost $12 each unless your filing fees were waived. Before the hearing date you should get together your list of problems, a copy of your lease if written, a copy of your notice letter, certified mail return receipt, the inspector’s report, any pictures or videos, and your rent receipts. When the case is heard, you will present your evidence first. The landlord or judge may ask you questions. Ask the inspector and your witnesses to testify after you. Then the landlord gets to present evidence and witnesses. You can question them about what they have said, but don't argue with them. If you do not come to court on your trial date, the court will dismiss your case. If you come to court and the other side does not, you should get a judgment. If both sides come to court, the judge will hear both sides and decide who wins. 


## RENT PAYMENT

The landlord is required to give a written receipt, upon request from the tenant, whenever the tenant pays rent in the form of cash or a money order. This is true by law, even if it’s not stated in a written lease. You should ALWAYS request a receipt every time you pay rent by cash or a money order.
 							
If you want an accounting of all the charges from the landlord and the payments you’ve made, you should make a written request to the landlord. The landlord is then required to give you a written statement showing all the charges and payments over the entire time of the tenancy, or the past 12 months, whichever is shorter. The landlord must provide this within 10 business days after receiving your request. 

--- 
# Eviction or Moving Out 
- [Breach of lease](#breach-of-lease)
- [End your lease](#ending-your-lease)
--- 

## BREACH OF LEASE

Fermentum et sollicitudin ac orci phasellus egestas tellus. Ut faucibus pulvinar elementum integer enim neque volutpat. Nunc lobortis mattis aliquam faucibus purus in. Fames ac turpis egestas maecenas pharetra convallis. Aliquam ut porttitor leo a diam sollicitudin tempor id. Velit egestas dui id ornare arcu odio ut. Amet consectetur adipiscing elit duis tristique sollicitudin nibh. Auctor eu augue ut lectus. Eu feugiat pretium nibh ipsum consequat nisl. Feugiat vivamus at augue eget arcu dictum varius duis. Turpis egestas pretium aenean pharetra magna ac placerat vestibulum. Vitae congue eu consequat ac. Nulla facilisi etiam dignissim diam quis enim lobortis scelerisque. Amet tellus cras adipiscing enim eu turpis egestas pretium aenean. Ipsum dolor sit amet consectetur adipiscing elit duis tristique. Tristique magna sit amet purus.

## ENDING YOUR LEASE

Whether your lease is written or just an oral agreement, there are certain procedures both you and your landlord must follow to properly end your tenancy.				
If you have no written lease and you pay rent by the month, the tenancy can be terminated by either you or the landlord for any reason or no reason at all, by giving at least 30 days written notice before the next rental due date. If you pay rent on a weekly basis, then it would be seven days notice.				
If you have a written lease, the notice requirements for termination should be contained in the lease. If it is a month to month lease, 30 days is usually required. If it is a year’s lease, the lease will usually state that you must give notice that you will not be renewing the lease 30 or 60 days before the lease ends. Often times, a year’s lease will change into a month to month lease after the year runs or it may renew automatically for another year. Check the lease carefully		

Remember, if you move out or are evicted before the lease term is up, then you can be held responsible for rent until the lease term expires or the unit is re-rented. For instance, if your lease runs through September and you are evicted or vacate the apartment in May without the landlord’s consent, you will still owe for June, July, August, and September. However, if the landlord re-rents the property, you would no longer owe rent for the months after the apartment is re-rented. A landlord cannot collect rent twice for the same property.

If the landlord sells the property, the tenant (lessee) has the same rights against the new owner (grantee) as s/he had against the original owner (grantor). However, the lease would be controlling. Read the lease carefully to see if it says anything about the tenant’s rights after the landlord sells the property. If the lease says nothing about a sale of the property, then the VRLTA applies and the tenant has all the rights usually granted by the law and the lease.			

There are special rules on notice of termination when the property you’re renting has been foreclosed on and there’s a new owner. If you have a lease and there are more than 90 days remaining on the lease, then you can’t be evicted until the end of the lease. However, if the new owner intends to use the property as his/her primary residence, then you can be given 90 days notice of termination. Also, you must be given 90 days notice if there is no lease, or if there is a lease with fewer than 90 days remaining, or if you have a month-to-month lease.	

There is also a rule that requires your landlord to give you notice if the property may be foreclosed on, even if foreclosure hasn’t occurred yet. It may happen that, even though you and the other tenants are paying your rent to the landlord, the landlord is not current in paying the mortgage on the property. The landlord is required to give you written notice of any of the following: the mortgage is in default, the landlord has received a notice of acceleration of the mortgage (that is, the lender is declaring the whole balance of the mortgage due now), or there’s an upcoming sale of the property upon foreclosure. The landlord must give you this notice within five business days after the landlord has received notice from the lender. If the landlord fails to give you this notice, you may terminate the lease by giving five days written notice to the landlord.	

## NOTICE OF EVICTION

Once your tenancy has been terminated by a proper notice, if you are still residing in the premises, the landlord can file an Unlawful Detainer (eviction) action in court seeking possession of the premises and any money you might owe. The sheriff will serve you with the Unlawful Detainer and it will state why the landlord wants you evicted, how much money they are claiming and the date and time of your court appearance. 
				
If you are being sued for nonpayment of rent and you pay to the landlord all the rent, and any late charges, court costs, and attorneys fees that are due and owing on or before judgment is entered, the Unlawful Detainer must stop the day you paid. You can only do this once in a 12 month period per landlord. 

At the court date, the judge will call your name and ask whether you admit or deny what the landlord said in his Unlawful Detainer. If you admit it, the court will enter judgment for possession of the property as well as a money judgment for the rent, damages, costs or fees sought by the landlord. The judge must give you 10 days before the landlord can get his writ of possession requiring that you leave.			

If you do not appear in court, the landlord can ask for an immediate writ of possession for that day. If granted, the sheriff will send you the writ as well as at least 72 hours notice of the date and time the actual eviction will occur.	

If you disagree with the landlord, the court will usually set another date to actually try the case. The judge will also ask both you and the landlord if you want the other side to put in writing why each feels they are right. The landlord’s writing is called a Bill of Particulars wherein he or she explains why they feel they are entitled to both possession and any money sought. You, in turn may be asked to write a Grounds of Defense explaining why you feel the landlord is wrong. The court will set dates when these are due to be sent to the other side and filed with the court. If they are not done by the required dates, you could automatically lose without ever presenting your case.

If you are covered by the VRLTA and ask the case to be set for another day, the landlord can ask the court that you pay all the rent owed to the court until the trial date. Unless you have a good defense to the case, the court will give you seven days to pay the money to the court. If you fail to make the payment on time, the landlord can ask for judgment without ever having the trial. If you are not covered by the VRLTA, the landlord cannot make this request. 					

When the case is heard, all witnesses will have to speak under oath. The landlord goes first. You will get a chance to question the landlord and any other witnesses. After that, you and your witnesses will testify. Then the landlord gets to ask questions. At the end, each side can make a short closing argument, telling all the reasons the judge should decide the case for the landlord or the tenant. After hearing both sides, the judge will decide the case. If you win, you can stay on as if the case never came to court. If the landlord wins, there will be a 10 day appeal period before the landlord can get the Writ of Possession. You can appeal the case to a higher court by filling out a notice of appeal in the clerk’s office within ten days of the judgment. However, you will also be required to pay an appeal bond within that same ten day period which would include all the money found owing to the landlord plus up to a year’s future rent.			

After the ten days runs, the landlord can get a writ of possession which will be served on you by the sheriff giving you at least 72 hours notice of the date and time the actual eviction will occur. The sheriff comes to keep the peace, and will not actually move you out. The landlord must take care of that. It is always best to move your things out before the sheriff arrives so as to avoid your belongings being placed on the curb.

Eviction can occur on the scheduled eviction day or any day after for a full year. If eviction does not occur, and the tenant pays the rent and other costs in full, the landlord may accept the rent with a written notice stating he or she is accepting the rent with reservation. This means the landlord is reserving the right to evict, based on the judgment, even though the tenant has paid in full. If the landlord does not give this notice, the landlord loses the right to evict without going to court again. 


[edit this page](https://gitlab.com/-/ide/project/netwit/hugo/blob/master/-/content/VA_info/va-tenant-rights.md)

