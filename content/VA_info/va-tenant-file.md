---
title: Infomration For Virginia Tenants

comments: True
---
# Contents

[Tenant's Assertion](#tenant-assertion)

[Warrent In Debt](#warrant-in-debt)

---
# Tenant Assertion
---

## Tools 

{{< button href="https://lawhelpinteractive.org/Interview/GenerateInterview/7163/engine" >}}Write Your Landlord a Letter and Fill Out A Form{{< /button >}} {{< button href="https://netwit.gitlab.io/hugo/va_info/ta-videos/" >}}Videos{{< /button >}}  {{< button href="https://netwit.gitlab.io/hugo/page/about" >}}Community{{< /button >}}  

## Info

| **1. Get Ready** | **2. File** | **3. What To Expext**|
| :-----------| :-----------|:-----------|
| 1. [You Must Be Current on Rent](Link) | 1.[What Do I Need To File?](Link) | 1. [What To Expect In Court](link) |
| 2. [You Must Give Written Notice To Your Landlord](#notify-your-landlord)| 2. [How do I Fill Out the Form?](#instructions)  | 2. [How do I file in Court](#file-at-your-local-district-court)|
| 3. [You must give them time to fix the issue](#give-them-time-to-fix-the-repair) | 3. [Where do I File the Form](link) | 3. [What to expect in Court](page/court_process)|
| 4. [Next Steps](Link) | 4. [I cannot afford to file this form](link) | 4. [How do I know what the Court Will Decide?](link)|

## What Is Tennant Assertion?

LeGrand Needs to Add A summary. 

## Steps 
1. [Notify Your Landloard](#notify-your-landlord.)

2. [Fill out a Tenant’s Assertion and Complaint](#tenant-assertion-and-complaint)

3. [File at your local District Court](#file-at-your-local-district-court)

4. [Set A Court Date](#set-a-court-date)


### 1. Notify Your Landlord. 

If your landlord is responsible for fixing the problem, **you must first notify the landlord in writing and give him or her a reasonable time to fix it.** 

### 2. Give them time to fix the repair

If you have notified the landlord of the problem but he or she has not made the necessary repairs within a reasonable time, you can take your landlord to court.


### 3. File a Tenant Assertion and Complaint Form

*  [Click Here for a Copy of the form](http://www.courts.state.va.us/forms/district/dc429.pdf) 

Summary: 


#### Instructions 
*  [Click here for instructions on how to fill out this form](/page/ta_instruction) 
   For more information on this form click here. 

*  For tips on filling on your legal issue [click here](https://www.youtube.com/watch?v=Ppm5_AGtbTo)

*  It will cost about $30 to file and serve the papers. You may ask the clerk for ["waiver of fee"](http://www.courts.state.va.us/forms/district/dc366a.pdf) if you can’t afford to pay.
*   When filing a Tenant’s Assertion, you must be completely current on your rent. Instead of sending your rent check to the landlord, you can send it to the court until repairs are complete. (Internal Note this languge needs to be made clearer)


### 4. File at your local District Court
*  You must file the form at the local district where you live. For a map of a Map of Virginia's Judicial Circuits and Districts [Click Here](http://www.courts.state.va.us/courts/maps/home.html). For case status and case infomration [click here](https://eapps.courts.state.va.us/gdcourts/captchaVerification.do?landing=landing)


#### 5. Set A Court Date
*  The court will set  hearing date and has the landlord served with a summons to appear in court.
*  For information on what to expect when dealing with the court [click here](page/court_process)

#### [What to Expect In Court](going-to-court)

---
# Warrent In Debt
--- 
Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Magna fringilla urna porttitor rhoncus dolor purus. Faucibus nisl tincidunt eget nullam non nisi est. Gravida dictum fusce ut placerat. Praesent tristique magna sit amet purus gravida. Eu augue ut lectus arcu bibendum at varius vel pharetra. Eu mi bibendum neque egestas congue quisque. Interdum velit euismod in pellentesque massa placerat duis ultricies lacus. Risus pretium quam vulputate dignissim suspendisse in. Ullamcorper eget nulla facilisi etiam dignissim diam quis enim. Nec tincidunt praesent semper feugiat nibh. Elementum nisi quis eleifend quam adipiscing vitae proin sagittis. A diam maecenas sed enim. Nec ultrices dui sapien eget mi proin. Sem viverra aliquet eget sit amet tellus cras adipiscing. Orci phasellus egestas tellus rutrum tellus pellentesque. Et ultrices neque ornare aenean euismod. Quis blandit turpis cursus in. Orci eu lobortis elementum nibh tellus molestie. At tellus at urna condimentum mattis pellentesque id nibh.

Odio ut enim blandit volutpat maecenas volutpat. Quam viverra orci sagittis eu. Iaculis at erat pellentesque adipiscing commodo elit at. In massa tempor nec feugiat nisl pretium fusce id. Viverra suspendisse potenti nullam ac tortor. Aliquet risus feugiat in ante metus. Magna fringilla urna porttitor rhoncus. Sit amet commodo nulla facilisi nullam vehicula ipsum. Quis vel eros donec ac odio tempor. Viverra justo nec ultrices dui sapien eget mi proin sed. Imperdiet sed euismod nisi porta lorem mollis aliquam. Ut faucibus pulvinar elementum integer enim neque volutpat ac tincidunt. Elit eget gravida cum sociis natoque penatibus et. Lectus urna duis convallis convallis tellus id interdum velit. Quisque egestas diam in arcu cursus. Vitae justo eget magna fermentum iaculis eu non diam.

Pulvinar pellentesque habitant morbi tristique senectus et netus et malesuada. Dui sapien eget mi proin sed libero enim. Diam maecenas sed enim ut sem viverra. Aliquam ultrices sagittis orci a scelerisque purus. A scelerisque purus semper eget duis at. Enim sit amet venenatis urna cursus eget. Nunc mi ipsum faucibus vitae aliquet nec ullamcorper sit. Diam phasellus vestibulum lorem sed risus ultricies tristique nulla aliquet. Etiam sit amet nisl purus in mollis nunc. Ac turpis egestas maecenas pharetra convallis posuere. Id donec ultrices tincidunt arcu. Semper risus in hendrerit gravida rutrum quisque non. Augue ut lectus arcu bibendum at varius vel pharetra. Ac turpis egestas maecenas pharetra convallis posuere morbi leo urna. Tellus orci ac auctor augue mauris augue. Viverra maecenas accumsan lacus vel facilisis volutpat est velit. Suspendisse potenti nullam ac tortor vitae purus faucibus. Nulla facilisi cras fermentum odio eu feugiat pretium nibh ipsum.

Eget aliquet nibh praesent tristique magna sit amet purus gravida. Eget aliquet nibh praesent tristique magna sit amet purus gravida. Cras sed felis eget velit aliquet sagittis. Lectus vestibulum mattis ullamcorper velit sed ullamcorper morbi tincidunt ornare. Eu scelerisque felis imperdiet proin fermentum leo vel orci porta. Vel eros donec ac odio tempor orci dapibus. Aliquet porttitor lacus luctus accumsan tortor posuere ac. Auctor neque vitae tempus quam pellentesque nec nam aliquam. Sit amet nisl purus in mollis nunc sed. Amet commodo nulla facilisi nullam vehicula ipsum a. Pellentesque nec nam aliquam sem et tortor consequat. Eros in cursus turpis massa tincidunt dui ut ornare. Malesuada nunc vel risus commodo viverra maecenas accumsan. Pulvinar etiam non quam lacus suspendisse faucibus. Sagittis nisl rhoncus mattis rhoncus urna neque. Felis eget velit aliquet sagittis id consectetur purus.

Hac habitasse platea dictumst quisque sagittis purus. Amet volutpat consequat mauris nunc congue. Nunc scelerisque viverra mauris in aliquam sem fringilla ut morbi. Integer feugiat scelerisque varius morbi enim nunc faucibus a pellentesque. Tincidunt lobortis feugiat vivamus at augue eget arcu. Massa massa ultricies mi quis hendrerit dolor magna eget. Viverra maecenas accumsan lacus vel facilisis. Dis parturient montes nascetur ridiculus. Quisque egestas diam in arcu cursus euismod quis. Scelerisque in dictum non consectetur a erat nam at. Pharetra diam sit amet nisl suscipit. Enim ut tellus elementum sagittis. Et sollicitudin ac orci phasellus egestas tellus. Est velit egestas dui id. Tempus urna et pharetra pharetra massa massa. Purus non enim praesent elementum. Commodo sed egestas egestas fringilla phasellus faucibus scelerisque eleifend. Molestie a iaculis at erat pellentesque adipiscing commodo elit. Dolor morbi non arcu risus quis varius quam.

Erat nam at lectus urna duis convallis convallis tellus id. Porttitor eget dolor morbi non arcu. Adipiscing tristique risus nec feugiat in fermentum. Sed vulputate mi sit amet. Pellentesque diam volutpat commodo sed egestas egestas fringilla phasellus faucibus. Aenean et tortor at risus. Eget felis eget nunc lobortis. Arcu non sodales neque sodales ut. Mattis aliquam faucibus purus in massa tempor nec feugiat. Arcu ac tortor dignissim convallis aenean et. Placerat in egestas erat imperdiet sed euismod nisi porta lorem.

Arcu risus quis varius quam quisque id diam vel. Donec adipiscing tristique risus nec. Duis convallis convallis tellus id interdum velit laoreet id. Felis donec et odio pellentesque diam volutpat commodo sed. Pellentesque pulvinar pellentesque habitant morbi tristique senectus et. Est placerat in egestas erat. Tempus urna et pharetra pharetra massa massa ultricies. Nascetur ridiculus mus mauris vitae ultricies. Facilisis gravida neque convallis a cras. Maecenas volutpat blandit aliquam etiam erat velit scelerisque in dictum. Eget est lorem ipsum dolor sit. Facilisis gravida neque convallis a. Turpis in eu mi bibendum neque. Turpis in eu mi bibendum neque egestas. Turpis massa sed elementum tempus egestas sed. Senectus et netus et malesuada fames ac. Ridiculus mus mauris vitae ultricies leo. Pretium quam vulputate dignissim suspendisse in est ante in. Vitae aliquet nec ullamcorper sit.

Eu non diam phasellus vestibulum. Eleifend quam adipiscing vitae proin. Velit egestas dui id ornare arcu odio ut sem. Ut lectus arcu bibendum at varius. Commodo sed egestas egestas fringilla phasellus. Aliquet risus feugiat in ante metus dictum at tempor commodo. Fringilla est ullamcorper eget nulla facilisi etiam dignissim. Varius morbi enim nunc faucibus a pellentesque sit amet. Tellus rutrum tellus pellentesque eu tincidunt tortor aliquam nulla. Adipiscing elit duis tristique sollicitudin nibh sit amet commodo.

Posuere ac ut consequat semper viverra nam. Mattis nunc sed blandit libero volutpat sed cras. Ornare quam viverra orci sagittis eu volutpat. Facilisis mauris sit amet massa vitae tortor. Amet risus nullam eget felis eget nunc lobortis. Ut eu sem integer vitae justo. Nunc vel risus commodo viverra maecenas accumsan lacus vel. Diam in arcu cursus euismod quis viverra. Risus commodo viverra maecenas accumsan lacus vel facilisis volutpat est. Turpis egestas sed tempus urna et pharetra. Elit at imperdiet dui accumsan sit amet nulla facilisi morbi. Suspendisse faucibus interdum posuere lorem ipsum dolor.

Lorem ipsum dolor sit amet consectetur adipiscing. Vestibulum sed arcu non odio euismod lacinia at quis. Duis at tellus at urna. Eget mi proin sed libero. In aliquam sem fringilla ut. Eget nullam non nisi est sit amet. Eget dolor morbi non arcu. Eget lorem dolor sed viverra ipsum nunc. In nisl nisi scelerisque eu ultrices vitae. Feugiat sed lectus vestibulum mattis ullamcorper velit. Risus sed vulputate odio ut enim blandit volutpat maecenas volutpat. Nec dui nunc mattis enim ut tellus elementum sagittis vitae.

---
# Going to court 
---

[Edit This Page](https://gitlab.com/-/ide/project/netwit/hugo/blob/master/-/content/VA_info/va-tenant-file.md)
