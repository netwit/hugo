--- 

title: Can I bring My Landlord To Court
subtitle: When Can I Bring Legal Action Against My Landlord? 
date: 2020-2-23
bigimg: [{src: "https://c.pxhere.com/photos/b4/74/hammer_books_law_court_lawyer_paragraphs_rule_jura-1330304.jpg!d"}]

---

| **When Can I Bring My LandLord To Court** |
| :-----------| 
| 1. [You Must Be Current on Rent](Link) | 
| 2. You Must Give Written Notice To Your Landlord| 
| 3. [You must give them time to fix the issue](more info) |
| 4. [If they don't respond you can bring them to court](Link to more info) |

