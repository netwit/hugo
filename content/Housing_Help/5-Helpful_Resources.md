---
title: Common Housing Questions and Resources
subtitle: Tenants Assertion Form
comments: False

---
## {{< button href="https://pbs.twimg.com/profile_images/3270439566/371bf29bd858c594ac8a5762dd4e24e1_400x400.jpeg" >}}Here is Proof of LeGrand's Lameness{{< /button >}}


{{< button href="https://www.valegalaid.org/resource/can-my-landlord-bar-my-guest-from-my-rental-p?ref=GUzxS" >}} Can My Landlord Bar My Guest From My Rental Property? {{< /button >}}


## {{< button href="https://www.valegalaid.org/resource/evictions-including-lockouts-and-utility-shut?ref=GUzxS" >}} Evictions {{< /button >}}

## {{< button href="https://www.valegalaid.org/resource/fair-housing/download/52BC7E17-BF1A-F6BE-8396-3D47B81A8404?ref=GUzxS" >}} Fair Housing {{< /button >}}

## {{< button href="https://www.valegalaid.org/resource/lawhelp-interactive-housing-law-interview/go/B15191C7-0BF8-5624-3351-724929913555?ref=GUzx" >}} Housing Forms {{< /button >}}
