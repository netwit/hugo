---
title: Court Tips 
subtitle: How to prepair and what to expect for a hearing
show-avatar: False
comments: False

---

## Prepairing for the hearng 

Before the hearing date you should get together your list of problems, a copy of your lease if written, a copy of your notice letter, certified mail return receipt, the inspector’s report, any pictures or videos, and your rent receipts.

## Day of Hearing 

**If you do not come to court on your trial date, the court will dismiss your case.** 

If you come to court and the other side does not, you should get a judgment. 

If both sides come to court, the judge will hear both sides and decide who wins. 

###### [Edit this page](https://gitlab.com/-/ide/project/netwit/hugo/blob/master/-/content/page/about.md)

###### [how to edit](https://about.gitlab.com/handbook/engineering/ux/technical-writing/markdown-guide/) 
