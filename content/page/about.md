---
title: About NetWit 
subtitle: Legal Git for Nitwits
comments: False

---
NetWit is an legal assistance platform, that aims to untilize the opensource and contributory nature of Git verson control to allow users the option to contribute, clone and comment on issues.

* [If you would like to clone this project click here.](hhttps://gitlab.com/netwit/hugo)

NetWit is the result of a combination of efforts by students at the University of Richomond School of Law. For information on the team please [click here.](https://netwit.gitlab.io/hugo/page/team/)


[Edit this page](https://gitlab.com/-/ide/project/netwit/hugo/blob/master/-/content/page/about.md)

[How to Edit](https://about.gitlab.com/handbook/engineering/ux/technical-writing/markdown-guide/) 


