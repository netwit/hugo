| **When Can I Bring My LandLord To Court** | **Getting Ready To File** | **What To Expext**|
| :-----------| :-----------|:-----------|
| 1. [You Must Be Current on Rent](Link) | 1.[What Do I Need To File?](Link To Form Page) | 1. [What To Expect In Court](link to court page) |
| 2. You Must Give Written Notice To Your Landlord| 2. [How do I Fill Out the Form?](Link to Instructions page)  | 2. [How do I file in Court](link to filing Page)|
| 3. [You must give them time to fix the issue](more info) | 3. [Where do I File the Form](link to page with instrcutions) | 3. [What to expect in Court](page/court_process)|
| 4. [If they don't respond you can bring them to court](Link to more info) | 4. [I cannot afford to file this form](link to filing fee and waiver) | 4. [How do I know what the Court Will Decide?](link to common legal arguments and decisions)| 
--- 
## Resource for Tenants
--- 
| **Tools** |  | |
| :-----------:| :-----------:|:-----------:|
| [Videos](page/videos) | [Wite your landlord a Letter](https://lawhelpinteractive.org/Interview/GenerateInterview/7163/engine) | [Community](page/community) |




